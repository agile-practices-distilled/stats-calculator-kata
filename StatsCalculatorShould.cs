using NUnit.Framework;

namespace StatsCalculator
{
    public class StatsCalculatorShould
    {
        [Test]
        public void show_minimum_number()
        {
            string result = StatsCalculator.Process(1, 0, -3, -10, 33, 665, -345, 2);
            
            Assert.IsTrue(result.Contains("MIN: -345"));
        }

        [Test]
        public void show_maximum_number()
        {
            string result = StatsCalculator.Process(888, 1, 5, 3, -10, 5, 5, 888, -734);

            Assert.IsTrue(result.Contains("MAX: 888"));
        }

        [Test]
        public void show_total_numbers_in_sequence()
        {
            string result = StatsCalculator.Process(3, 3, 4, 1, 2, 4, 7, 9, 7);

            Assert.IsTrue(result.Contains("TOTAL: 9"));
        }

        [Test]
        public void show_average_value()
        {
            string result = StatsCalculator.Process(2, 3, 2, 1, 5, 5, -10, 0);

            Assert.IsTrue(result.Contains("AVG: 1"));
        }
    }
}