﻿using System;
using System.Text;

namespace StatsCalculator
{
    public class StatsCalculator
    {
        public static string Process(params int[] input)
        {
            int min = Int32.MaxValue;
            int max = Int32.MinValue;
            int totalValue = 0;
            
            foreach (var number in input)
            {
                if (number < min)
                    min = number;
                if (number > max)
                    max = number;
                totalValue += number;
            }

            var result = new StringBuilder();
            result.AppendLine($"MIN: {min}");
            result.AppendLine($"MAX: {max}");
            result.AppendLine($"TOTAL: {input.Length}");
            result.AppendLine($"AVG: {totalValue/input.Length}");
            return result.ToString();
        }
    }
}
